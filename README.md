# Data resulting from the evaluation of the DMU Frwmework
The DMU (Declarative Microservices Update) framework automates the update of multi-microservices on multi PaaS sites.
This repository stores the data associated to the evaluation of the [DMU framework prototype](https://github.com/tao-xinxiu/prototype-template-engine) with three usecases: Lizard, Account and HelloWorld.

## Evaluation data storage

Two git repositories were used to store evaluation data files (due to size limit associated to gitlab account).

The first one is [experiments data (Lizard strategies)](https://gitlab.com/xxtao/experiment/tree/master/data)  
This directory contains experiments related to strategies only on Lizard use-case.

The second one is [experiments data (Lizard failures & Account strategies)](https://gitlab.com/xxtao/eval-data/)  
This directory contains experiments related to a) failure experiments on Lizard and b) strategies evaluation on usecase Account.

Note : no evaluation were carried out on failures on Account


## Experiment output raw data file (structure)

Three metrics are monitored during the update: update duration, resource consumption, and microservices availability.
The experiments output the following data files to correspond to each metric. 
The directory contains three kinds of data files.
The naming of the different file types is explained above :
- update duration: `duration_prototype_[STRATEGY_INITIAL](_[FAIL_TYPE])_[USECASE]_[TIMESTAMP].csv`
- resource consumption: `resource_prototype_[STRATEGY_INITIAL](_[FAIL_TYPE])_[USECASE]_[TIMESTAMP].csv`
- microservices availability: `[MICROSERVICE_NAME]_[PAAS_SITE](_[FAIL_TYPE])_[USECASE]_[TIMESTAMP].csv`

To facilitate the following summarization of the experiment result, perform the command `./summary/history/rename_jmeter.sh` to make all the output filename about microservices availability to start with "jmeter_". 


